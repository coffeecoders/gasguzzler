package com.example.gasguzzler;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

// quick file change test
public class MainActivity extends ListActivity {
	
	public final static String EXTRA_MESSAGE = "com.example.GasGuzzler.MESSAGE";
	
	TextView selection;
	String[] cars={"ford","jetta"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// bind data from array to the list in the ListActivity
		setListAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,cars));
	}
	
	public void onListItemClick(ListView parent, View v, int position, long id) {
		String message = cars[position];
		
		// create a new intent with information to pass to next activity
		// this passes the list of cars so that the correct info can be shown
		Intent intent = new Intent(this, DisplayResultActivity.class);	    	  
		intent.putExtra(EXTRA_MESSAGE, message);
		
	    startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_activity_actions, menu);
	    return super.onCreateOptionsMenu(menu);
	}

}
